package com.m3.training.constructors;

public class Animal implements IAnimal  {

	public Animal() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.m3.training.constructors.IAnimal#eat()
	 */
	@Override
	public String eat() {
		return "I am an animal and I eat.";
	}
	
	public int doRandomThing() {
		return 88;
	}
}
