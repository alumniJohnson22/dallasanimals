package com.m3.training.constructors;

public interface IBark {

	String bark();

}