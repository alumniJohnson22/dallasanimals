package com.m3.training.constructors;

public class Driver {

	public static void main(String[] args) {
	
		Object obj = new Object();
		IAnimal animal = new Animal();
		animal.eat();
		//animal.doRandomThing();
		Dog dog = new Dog();
		
		System.out.println(obj.toString());
		System.out.println(animal.toString());
		System.out.println(dog.toString());
		
		IAnimal animalo = new Dog();
		
		obj = "Hello";
		obj = animalo;
		obj = dog;
		
		animalo = dog;
		//.....
		animalo = new Tiger();
		// .........
		if (animalo instanceof Dog) {
			System.out.println(((IBark)animalo).bark());
		}
		
		ShibaInu doge = new ShibaInu();
		System.out.println(doge.bark());
		System.out.println("driver class in same package can use shibainu ref to get protected number:");
		System.out.println(doge.getProtectedNumber());
		System.out.println("done with protected number");
		System.out.println();
		
	}

}
