package com.m3.training.constructors;

public interface IAnimal {

	String eat();

}