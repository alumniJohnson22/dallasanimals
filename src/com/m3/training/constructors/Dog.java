package com.m3.training.constructors;

public class Dog extends Animal {

	protected int getProtectedNumber() {
		return 5;
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.constructors.IBark#bark()
	 */

	
	@Override
	public String eat() {
		return "I am a dog and I eat.";
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.constructors.IFetch#fetch()
	 */


}