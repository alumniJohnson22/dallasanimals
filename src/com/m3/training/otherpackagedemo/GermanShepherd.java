package com.m3.training.otherpackagedemo;

import com.m3.training.constructors.Dog;
import com.m3.training.constructors.IBark;
import com.m3.training.constructors.IFetch;

public class GermanShepherd extends Dog implements IBark, IFetch {

	public GermanShepherd() {
		System.out.println("Hello I am a German Shepherd. My private number is " + super.getProtectedNumber());
	}

	@Override
	public String eat() {
		String msg = super.eat();
		return msg + " I am a German Shepherd!";
	}
	
	public String bark() {
		return "roof " + getProtectedNumber();
	}
	
	public String fetch() {
		return "I got that stick for you";
	}
}
